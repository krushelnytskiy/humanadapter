Cms2Cms Human Adapter

Засіб для міграції з непідтримуваних Cms/платфом

# Опис

## Можливості

Даний адаптер здатний виконувати операції на стороні клієнтського сайту, а також повертати їх, як вихід. 

Основні дані, це: 

1. Кількість даних;

2. Дані у вигляді JSON (всі або конкретний тип сутності);

3. Список ідентифікаторів сутностей (всі або конкретний тип сутності).

До можливостей адаптера також відноситься вільне керування даними через файл конфігурації, в залежності від того, з якої платформи здійснюється читання. Цей файл містить інформацію про: 

1. Типи сутностей;

2. Адресу адмін панелі;

3. Дані про посилання на різні типи сутностей;

4. Дані про  ідентифікатори полів сутностей;

5. Дані функцій, що виконується при виявленні сутності.

	

## Вимоги

	Використання адаптера містить наступні вимоги:

1. Втановлено CasperJs на базі PhantomJs > 1.9.1;

2. Запуск скрипта у директорії виконуваного файлу;

3. Правильна постановка аргументів та даних в конфігураційному файлі;

4. Використання HTML-типної Адмін-Панелі.

## Теоретичні відомості

	**Ідентифікатори полів сутностей** - це посилання/селектор для окремого поля сутності, з якого можна витягнути значення.

	**Селектор** -  це формальний опис того елемента або групи елементів, до яких застосовується зазначене правило стилю, також служить для ідентифікації DOM елементів в JavaScript.

	**Директорія виконуваного файлу **- це директорія, що в проекті Cms2Cms міститься в *bin/phantomjs/human/ *і містить таке дерево файлів

* human

    * data/

        * ...

    * lib/

        * do_while.js

    * action_manager.js

    * engine.js

    * messenger.js

	

яке є необхідним для життєвого циклу адаптера.

# Застосування

## Можливості

Адаптер можна застосовувати, як для стандартних типів сутності (post, page, comment, category), так і для повністю абстрактних сутностей. Також ідентифікаторів поля сутності не обов’язково вказувати на тег input, чи елемент форми. Ідентифікатор сутності може набирати таких виглядів:

1. Відношення селекторів (mapping_selectors_relations)

2. Відношення посилання (mapping_url_relations)

3. Відношення "multiply" (mapping_multiply_relations)

4. Спеціальні відношення (mapping_custom_relations) 

	Також можна застосовувати розбір списку сутностуй, як з використанням пагінації, так і без.

	

Детільніше про можливості застосування в п. 4.

## Вимоги

	Для коректної роботи адаптера потрібно правильно застосовувати конфігураційний файл і враховувати ресурси сервера (На 1000 сторінок середнього  розміру приходиться навантаження на сервер, так, як 1000 відкритих пустих вкладок десктопного інтернет браузера). Для правильного застосування ідентифікаторів полів сутностей  потрібно визначити максимально чіткий селектор. 

В разі виявлення дублікатів одного поля сутності, адаптер поверне значення першого поля, що співпадає з селектором.

## Практичне застосування

	Запуск виконуваного файлу *engine.js *з директорії виконуваного файлу здійснюється за допомогою команди, типу:

casperjs engine.js joomla_joomla1 (NOTE:  Source Cms Name) parse-users (NOTE:  Action [action]-[сутність]) [http://m.aga.tk/j10](http://m.aga.tk/j10) (NOTE:  Source URL) admin (NOTE:  Адмін Панель Логін ) pass123 (NOTE:  Адмін панель Пароль)

Всі аргументи, що вказано, мають входити в порядку, і відсутність одного з них приведе по помилки. Тепер розглянемо, що робить консольна команда:

1. casperjs engine.js - ця частина команди запускає скрипт engine.js за допомогою тестової утиліти CasperJs( детальніше: [http://casperjs.org](http://casperjs.org/) )

2. joomla_joomla1 - скорочена назва назви Source CMS. В скрипті служить, як посилання до файлу конфігурації. Тобто для таких назв потрібно такі конфігураційні файли:

	

<table>
  <tr>
    <td>Name</td>
    <td>Path</td>
  </tr>
  <tr>
    <td>joomla_joomla1</td>
    <td>/data/joomla/joomla1.js</td>
  </tr>
  <tr>
    <td>expressionengine_expressionengine3</td>
    <td>/data/expressionengine/expressionengine3</td>
  </tr>
</table>


3. parse-users - атрибут, який визначає тип даних і дані, що потрібно повернути. Існує 3 основні дії: (**counts**, **parse**, **grabitems**). Дефісом розділяється дія та тип сутності. Якщо, скажімо у конфігураційному файлі є такі сутності: "comments", “users”, “posts”, то дія parse виконає дію parse для всіх сутностей, а дія parse-users тільки для сутності users.

4. [http://m.aga.tk/j10](http://m.aga.tk/j10) - посилання на сайт, де розміщена source Cms

5. admin - логін для Адмін Панелі source Cms

6. pass123 - пароль для Адмін Панелі source Cmse

Тепер розглянемо конфігураційний файл. Це простий javascript файл, що містить змінну **details**. Це основна змінна конфігурації, і виконуваний файл не зможе продовжити роботу без цієї змінної.

var details = {}; 

Тепер розглянемо структуру і можливості змінною **details. **

Основні ключі - це: 

* details.resources - це типи сутностей, які нам потрібно витягнути з source Cms (в типів сутностей може бути довільна назва).

	Приклад:

		…

		resources: [ "posts", "categories", "comments", "users" ],

		...

* details.urls - це масив посилань на списки сутностей. Наприклад для сутності users:

	.....

var details = {

urls: {

		posts: {

		…

},

users**:** {
      		base**:** "/index2.php?option=com_users",
      		pagination**:** "&limitstart=calc(calc(##-1)*30)"
    	},

…

}

}

	Ключ **base **зберігає значення посилання на список.

	Ключ **pagination** зберігає значення підстановки параметру пагінації(якщо потрібно), з ключем макропідстановки **"##" **і дорівнює номеру сторінки списку сутностей.

* details.selectors - це масив селекторів-ідентифікаторів, містить всі можливі селектори для визначення сутності в списку між іншими DOM-елементами. Приклад для сутності users:

	var details = {

…

		selectors: {

…

			users**:** {
     			    	"global-selector"**:** "table.adminlist",
      				"item-selector"**:** "tr[class^=\"row\"]",
      				"link-selector"**:** "a[href*=\"option=com_users\"]"
    			}

…

},

…

}

	Ключ **global-selector** містить дані селектора таблиці, списку, чи блоку, який містить список сутностей. Не є остаточним  селектором для сутності, тому можна залишити пустим, що не є рекомендовано. 

	Ключ **item-selector** містить дані селектора рядка таблиці, елементу списку чи блоку, що містить елемент сутності в списку. Наслідує селектор **global-selector**.

	Ключ **global-selector **містить дані селектора посилання на форму редагування сутності і наслідує **item-selector**.

* details.login_elements - масив ключів - значень, що містить селектори полів потрібні для входу у Панель Адміністратора. Приклад застосування:

	

var details = {

login_elements**:**  {
    		login_form**:** 'form[action="index.php"][name="loginForm"]',
    		username**:** 'input[type="text"][name="usrname"]',
    		password**:** 'input[type="password"][name="pass"]',
    		submit**:** 'input[type="submit"][name="submit"]'
  	}

}

	Ключ **login_form **містить селекор форми, де яку потрібно заповнити доступами до панелі адміністратора, а username, password і submit містять селектори полів, куди потрібно вводити доступа і селектор кнопки відправлення, відповідно. 

* details.limits - масив ключів - значень, що містить інформацію, про кількість сутностей на одній сторінці пагінації. Приклад: 

   		var details = {

 limits**:** {
    content**:** 30,
    categories**:** 30,
    users**:** 30,
    menus**:** 30
  }

}

	Ключ - це назва сутності, значення - кількість сутностей, на одній сторінці пагінації. Якщо в панелі адміністратора не використовується пагінація для сутності, то використовувати конфігурацію не потрібно.

	

* details.entity - об’єкт, який містить інформація про сутність, формує його поля в формат відповіді і визначає зв’язки селекторів і полів, зв’язки користувацьких функцій, тощо. Приклад застосування:

var details = {

entity**:** {
    content**:** {
      mapping_url_relations**:** {
        id**:** 'id'
      },

      mapping_selectors_relations**:** {
        title**:** 'form[action="index2.php"] input[name="title"]',
        title_alias**:** 'form[action="index2.php"] input[name="title_alias"]',
        sectionid**:** 'form[action="index2.php"] select[name="sectionid"]',
        introtext**:** 'form[action="index2.php"] iframe#introtext_ifr',
        fulltext**:** 'form[action="index2.php"] iframe#fulltext_ifr',
        meta_description**:** 'form[action="index2.php"] iframe#metadesc_ifr',
        meta_keywords**:** 'form[action="index2.php"] iframe#metakey_ifr',
        created_by**:** 'form[action="index2.php"] select[name="created_by"]',
        created**:** 'form[action="index2.php"] input[name="created"]'
      },

	mapping_multiply_relations**:** {
                has_many**:** {
                    categories**:** {
                        __return__**:** **function** () {
                            **return** [].slice.call(document.querySelectorAll('#sub_hold_field_category input[type="checkbox"]:checked')).map(**function**(cat) {**return** cat.value});
                        }
                    }
                }
            },

            mapping_custom_relations**:** {
                featured_image**:** {
                    __return__**:** **function** () {
                        **return** jQuery('#field_id_10').find('li').data('file-url');
                    }
                }
            }

    },

}

Пояснення: проекція об’єкта складається з 4 частин:

1. **mapping_url_relations** -  проекція частин посилання на об’єкт сутності

    1. ключ: назва поля сутності

    2. значення: **параметр** з стрічки посилання 

2. **mapping_selectors_relations **- проекція селекторів DOM-елементів на об’єкт сутності

3. **mapping_multiply_relations **- проекція на даний час(3 Червня 2015) підтримує тільки проекцію типу "має багато". Використовується у випадках, коли одна сутність у формі редагування містить більше одного значення. Для кожної сутності потрібно написати функцію **__return__ **, яка буде повертати значення або масив значень з потрібним полем.

4. **mapping_custom_relations **- користувацька проекція. Якщо потрібно дістати дані із нетипового поля або виконати операцію перед витягуванням поля сутності, тоді потрібно для поля написати функцію **__return__**, яка буде повертати значення і додасть до сутності

 

* details.admin_path - стрічка, яка містить піддиректорію, що веде до панелі адміністратора.

		Приклад: 

		var details = {

			admin_path = '/administrator'

}

# Структура

Адаптер складається з такого дерева файлів:

* human

    * data/

        * ...

    * lib/

        * do_while.js

    * action_manager.js

    * engine.js

    * messenger.js

В директорії **data** здерігатимуться конфігураційні файли, в директорії **lib **сторонні скрипти. Скрипт **action_manager.js** приймає вхідні параметри від **engine.js**, який збирає аргументи в потрібному порядку,  входить в панель адміністратора і імпортує необхідні для життєвого циклу файли. Весь результат виводиться за допомогою скрипта **messenger.js**, який формує JSON відповідь.

	

# Для розробників

	Ідеального коду не буває, тому скоріше за все потрібно буде дописувати і виправляти Адаптер. 

Так, як JavaScript працює асинхронно для того, щоб код чекав на відповідь і працював послідовно було використано Promises - шаблон програмування(більше [https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Promise](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Promise)),який використовується в casperJs так: 

casper.start();

	casper.then( function() { go(forward); } );

	casper.then( function() { flip(back); } );

В даному випадку функція **flip**  не почне виконуватися, доки не буде результат від функції **go.** Це дає нам змогу використати цикл у функції **go** і не втратити послідовність виконання.

Якщо слідкувати за змінною **casper** в коді casperJs скрипта, то можна використовувати методи для змінної **casper**, яка є модулем і містить методи для роботи з  Promises. наприклад

* [then()](http://docs.casperjs.org/en/latest/modules/casper.html#then)

* [thenBypass()](http://docs.casperjs.org/en/latest/modules/casper.html#thenbypass)

* [thenBypassIf()](http://docs.casperjs.org/en/latest/modules/casper.html#thenbypassif)

* [thenBypassUnless()](http://docs.casperjs.org/en/latest/modules/casper.html#thenbypassunless)

* [thenClick()](http://docs.casperjs.org/en/latest/modules/casper.html#thenclick)

* [thenEvaluate()](http://docs.casperjs.org/en/latest/modules/casper.html#thenevaluate)

* [thenOpen()](http://docs.casperjs.org/en/latest/modules/casper.html#thenopen)

* [thenOpenAndEvaluate()](http://docs.casperjs.org/en/latest/modules/casper.html#thenopenandevaluate)

та багато інших, яких можна знайти [тут](http://docs.casperjs.org/en/latest/modules/casper.html)