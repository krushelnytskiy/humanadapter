/**
 * Created by v.krushelnytskiy on 4/8/15.
 */

var inject = [];

var casper = require("casper").create({
    verbose: true,
    loglevel: "debug"
});

var system = require("system");

args_flip = function ( offset ) {
    if(!offset) {
        offset = 0;
    }

    return system.args[system.args.length-(1+offset)];
};

inject.messenger = phantom.injectJs("./messenger.js");
inject.lib__do_while  = phantom.injectJs('./lib/do_while.js');
var action = args_flip(3).split('-')[0];
var actionParams = args_flip(3).split('-');
actionParams.shift();
var sourceCmsDetails = './data/' + args_flip(4).replace('_', '/') + '.js';
var url = args_flip(2);
var credentials = {
    username: args_flip(1),
    password: args_flip()
};

inject.details = phantom.injectJs(sourceCmsDetails);
inject.default_parser = phantom.injectJs('./data/default.js');
inject.action_manager = phantom.injectJs('./action_manager.js');


if( inject.indexOf(false) != -1) {
    Messenger.error.terminate(1);
}

var action_manager = new ActionManager(action, actionParams);

casper.start(url + details.admin_path, function ( load_info ) {

    if(['4','5'].indexOf(load_info.status.toString()[0]) != -1 ) {
        Messenger.error.terminate(0);
    }
});

casper.then(function () {
   this.waitForSelector(details.login_elements.login_form);
});

casper.then(function () {
    var login_selectors_values = {};
    login_selectors_values[details.login_elements.username] = credentials.username;
    login_selectors_values[details.login_elements.password] = credentials.password;
    this.fillSelectors(details.login_elements.login_form, login_selectors_values, true);
});

casper.then(function step1() {
    action_manager.run();
});

casper.then(function step2() {
    action_manager.echo_answer();
});

casper.onErrorMessage = function (msg) {
    Message.error.print(msg);
};


casper.onError = function (msg) {
    Message.error.print(msg);
};

casper.run();