var details = {
  name: "Joomla",
  version: "1.0",
  resources: ["content", "categories", "sections", "users", "menus"],

  urls: {
    content: {
      base: "/index2.php?option=com_content&sectionid=0",
      pagination: "&limitstart=calc(calc(##-1)*30)"
    },

    categories: {
      base: "/index2.php?option=com_categories&section=content",
      pagination: "&limitstart=calc(calc(##-1)*30)"
    },

    sections: {
      base: "/index2.php?option=com_sections&scope=content",
      pagination: "&limitstart=calc(calc(##-1)*30)"
    },
    users: {
      base: "/index2.php?option=com_users",
      pagination: "&limitstart=calc(calc(##-1)*30)"
    },

    menus: {
      base: "/index2.php?option=com_menumanager",
      pagination: "&limitstart=calc(calc(##-1)*30)"
    },

    menuitems: {
      base: "/index2.php?option=com_menus&menutype=##menutype##",
      pagination: "&limitstart=calc(calc(##-1)*30)"
    }
  },

  selectors: {
    content: {
      "global-selector": "table.adminlist",
      "item-selector": "tr[class^=\"row\"]",
      "link-selector": "a[href*=\"option=com_content\"]"
    },

    categories: {
      "global-selector": "table.adminlist",
      "item-selector": "tr[class^=\"row\"]",
      "link-selector": "a[href*=\"option=com_categories\"]"
    },

    sections: {
      "global-selector": "table.adminlist",
      "item-selector": "tr[class^=\"row\"]",
      "link-selector": "a[href*=\"option=com_sections\"]"
    },

    users: {
      "global-selector": "table.adminlist",
      "item-selector": "tr[class^=\"row\"]",
      "link-selector": "a[href*=\"option=com_users\"]"
    },

    menus: {
      "global-selector": "table.adminlist",
      "item-selector": "tr[class^=\"row\"]",
      "link-selector": "a[href*=\"option=com_menumanager\"]"
    },

    menuitems: {
      "global-selector": "table.adminlist",
      "item-selector": "tr[class^=\"row\"]",
      "link-selector": "a[href*=\"option=com_menus\"]"
    }
  },
  
  login_elements:  {
    login_form: 'form[action="index.php"][name="loginForm"]',
    username: 'input[type="text"][name="usrname"]',
    password: 'input[type="password"][name="pass"]',
    submit: 'input[type="submit"][name="submit"]'
  },

  limits: {
    content: 30,
    categories: 30,
    sections: 30,
    users: 30,
    menus: 30
  },

  entity: {
    content: {
      mapping_url_relations: {
        id: 'id'
      },

      mapping_selectors_relations: {
        title: 'form[action="index2.php"] input[name="title"]',
        title_alias: 'form[action="index2.php"] input[name="title_alias"]',
        sectionid: 'form[action="index2.php"] select[name="sectionid"]',
        introtext: 'form[action="index2.php"] iframe#introtext_ifr',
        fulltext: 'form[action="index2.php"] iframe#fulltext_ifr',
        meta_description: 'form[action="index2.php"] iframe#metadesc_ifr',
        meta_keywords: 'form[action="index2.php"] iframe#metakey_ifr',
        created_by: 'form[action="index2.php"] select[name="created_by"]',
        created: 'form[action="index2.php"] input[name="created"]'
      }
    },

    categories: {
      mapping_url_relations: {
        id: 'id'
      },

      mapping_selectors_relations: {
        title: 'form[action="index2.php"] input[name="title"]',
        name: 'form[action="index2.php"] input[name="name"]',
        section: 'form[action="index2.php"] input[name="section"]',
        image: 'form[action="index2.php"] select[name="image"]',
        published: 'form[action="index2.php"] input[name="published"]:checked',
        decription: 'form[action="index2.php"] iframe#description_ifr'
      }
    },

    sections: {
      mapping_url_relations: {
        id: 'id'
      },

      mapping_selectors_relations: {
        title: 'form[action="index2.php"] input[name="title"]',
        name: 'form[action="index2.php"] input[name="name"]',
        published: 'form[action="index2.php"] input[name="published"]:checked',
        description: 'form[action="index2.php"] iframe#description_ifr'
      }
    },

    users: {
      mapping_url_relations: {
        id: 'id'
      },

      mapping_selectors_relations: {
        name: 'form[action="index2.php"] input[name="name"]',
        username: 'form[action="index2.php"] input[name="username"]',
        email: 'form[action="index2.php"] input[name="email"]',
        blocked: 'form[action="index2.php"] input[name="block"]:checked'
      }
    },

    menus: {
      mapping_selectors_relations: {
        menutype: 'table.adminform input[name="menutype"]'
      },

      child_nodes: {
        menuitems: {
          mapping_external_relations: {
            menutype: 'menutype'
          }
        }
      }
    }
  },

  admin_path: '/administrator'
};