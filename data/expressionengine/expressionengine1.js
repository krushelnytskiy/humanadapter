/**
 * Created by v.krushelnytskiy on 4/22/15.
 */

var details = {
    name: "Expression Engine",
    version: "1.0",
    resources: ["blogs" ,"comments"],

    urls: {
        blogs: {
            base: "/index.php?D=cp&C=addons_modules&M=show_module_cp&module=zenbu&channel_id=7",
            pagination: "&perpage=calc(##*25)"
        },

        comments: {
            base: '/index.php?D=cp&C=addons_modules&M=show_module_cp&module=comment',
            pagination: "s"
        },

        categories: {
            base: "/index2.php?option=com_categories&section=content",
            pagination: "&limitstart=calc(calc(##-1)*30)"
        }
    },

    selectors: {
        blogs: {
            "global-selector": "table.mainTable.resultTable",
            "item-selector": "tr.entryRow",
            "link-selector": "a.zenbu_entry_form_link[href*=\"index.php\"]"
        },

        categories: {
            "global-selector": "table.adminlist",
            "item-selector": "tr[class^=\"row\"]",
            "link-selector": "a[href*=\"option=com_categories\"]"
        }
    },

    login_elements:  {
        login_form: 'form[action*="index.php"][method="post"]',
        username: 'input[type="text"][name="username"]',
        password: 'input[type="password"][name="password"]',
        submit: 'input[type="submit"][name="submit"]'
    },

    limits: {
        blogs: 25,
        categories: 25,
        sections: 30,
        users: 30,
        menus: 30
    },

    entity: {
        blogs: {
            mapping_url_relations: {
                id: 'entry_id'
            },

            mapping_selectors_relations: {
                title: 'form#publishForm[action*="index.php"] input[name="title"]',
                url_title: 'form#publishForm[action*="index.php"] input[name="url_title"]',
                structure__uri: 'form#publishForm[action*="index.php"] input[name="structure__uri"]',
                intro: 'form#publishForm[action*="index.php"] textarea[name="field_id_9"]',
                content: 'form#publishForm[action*="index.php"] textarea[name="field_id_11"]',
                created: 'form#publishForm[action*="index.php"] input[name="entry_date"]'
            },


            mapping_multiply_relations: {
                has_many: {
                    categories: {
                        __return__: function () {
                            return [].slice.call(document.querySelectorAll('#sub_hold_field_category input[type="checkbox"]:checked')).map(function(cat) {return cat.value});
                        }
                    }
                }
            },

            mapping_custom_relations: {
                featured_image: {
                    __return__: function () {
                        return jQuery('#field_id_10').find('li').data('file-url');
                    }
                }
            }
        },

        categories: {
            mapping_url_relations: {
                id: 'id'
            },

            mapping_selectors_relations: {
                title: 'form[action="index2.php"] input[name="title"]',
                name: 'form[action="index2.php"] input[name="name"]',
                section: 'form[action="index2.php"] input[name="section"]',
                image: 'form[action="index2.php"] select[name="image"]',
                published: 'form[action="index2.php"] input[name="published"]:checked',
                decription: 'form[action="index2.php"] iframe#description_ifr'
            }
        },

        sections: {
            mapping_url_relations: {
                id: 'id'
            },

            mapping_selectors_relations: {
                title: 'form[action="index2.php"] input[name="title"]',
                name: 'form[action="index2.php"] input[name="name"]',
                published: 'form[action="index2.php"] input[name="published"]:checked',
                description: 'form[action="index2.php"] iframe#description_ifr'
            }
        },

        users: {
            mapping_url_relations: {
                id: 'id'
            },

            mapping_selectors_relations: {
                name: 'form[action="index2.php"] input[name="name"]',
                username: 'form[action="index2.php"] input[name="username"]',
                email: 'form[action="index2.php"] input[name="email"]',
                blocked: 'form[action="index2.php"] input[name="block"]:checked'
            }
        },

        menus: {
            mapping_selectors_relations: {
                menutype: 'table.adminform input[name="menutype"]'
            },

            child_nodes: {
                menuitems: {
                    mapping_external_relations: {
                        menutype: 'menutype'
                    }
                }
            }
        }
    },

    admin_path: '/system'
};
