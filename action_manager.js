/**
 * Created by v.krushelnytskiy on 4/8/15.
 */

function ActionManager(action, params) {
    this.action = action;
    this.params = params;
    this.answer_value = {};

    this.run = function () {
        var func = 'run_' + this.action;

        if (this.hasOwnProperty(func)) {
            this[func]();
        } else {
            Messenger.info.print_plain("Usage: casperjs [executable].js <SOURCE_CMS> <ACTION> <URL> <ADMIN LOGIN> <ADMIN PASSWORD>\nActions:\n" + ActionManager.print_actions_description());
            phantom.exit();
        }
    };

    this.echo_answer = function () {
        Messenger.info.print(this.answer_value);
    };

    this.run_resources = function () {
        this.answer_value = details.resources;
    };

    this.run_counts = function () {
        that = this;
        casper.each(details.resources, function (self, resource) {
            that.get_entities(self, resource, true);
        });
    };

    this.run_grabitems = function () {
        that = this;

        resource_name = this.params[0];

        if(!resource_name) {
            Messenger.terminate(0);
        }

        that.get_entities(casper, resource_name, false);
    };

    this.run_parse = function () {
        resource_name = this.params[0];

        if (!resource_name) {
            Messenger.error.terminate(0);
        }

        that = this;
        casper.then(function () {
            that.run_grabitems(resource_name);
        }).then(function() {
             resources_to_parse =
                 ( ActionManager.is_numeric(that.params[1]) && ActionManager.is_numeric(that.params[2]) ) ?
                 that.answer_value[resource_name].items.slice(that.params[1], that.params[2]) :
                 that.answer_value[resource_name].items ;

        }).then(function () {
            that.answer_value = [];
            casper.each(resources_to_parse, function (self, item) {
                self
                .then(function () {
                    item_url = details.entity[resource_name].get_entity_url ? details.entity[resource_name].get_entity_url(item) : default_details.get_entity_url(item);
                })
                .then(function () {
                    self.open(item_url);
                })
                .then(function () {
                        self.each(Object.values(details.entity[resource_name].mapping_selectors_relations), function (self_self, selector) {
                            self.waitForSelector(selector);
                        })
                })
                .then(function () {
                    entity = this.evaluate(function (entity_query_selectors_mapping) {
                        var entity_value = {};


                        function steal_content(dom_element) {
                            switch (dom_element.tagName) {
                                case "P"     :
                                case "A"     :
                                case "DIV"   :
                                case "SPAN"  :
                                case "H1"    :
                                case "H2"    :
                                case "H3"    :
                                case "H4"    :
                                case "H5"    :
                                case "H6"    :
                                case "B"     :
                                case "STRIKE":
                                    return dom_element.innerText;
                                case "SELECT":
                                case "INPUT" :
                                case "TEXTAREA":
                                    return dom_element.value;
                                case "IFRAME":
                                    return dom_element.contentDocument.body.innerText;
                                default      :
                                    return dom_element.innerText;
                            }
                        }
                        for (var name in entity_query_selectors_mapping) {
                            entity_value[name] = steal_content(document.querySelector(entity_query_selectors_mapping[name]));
                        }
                        return entity_value;

                    }, details.entity[resource_name].mapping_selectors_relations);
                })
                .then(function () {
                    for(var i in details.entity[resource_name].mapping_url_relations) {
                        entity[i] = ActionManager.get_url_vars(this.getCurrentUrl())[details.entity[resource_name].mapping_url_relations[i]];
                    }
                })
                .then(function () {
                     if(details.entity[resource_name].hasOwnProperty('mapping_multiply_relations')) {
                         if(details.entity[resource_name].mapping_multiply_relations.hasOwnProperty('has_many')) {
                             entity.has_many = {};
                             for(var name in details.entity[resource_name].mapping_multiply_relations.has_many) {
                                 entity.has_many[name] = this.evaluate(details.entity[resource_name].mapping_multiply_relations.has_many[name].__return__);
                             }
                         }
                     }
                })
                .then(function() {
                        if(details.entity[resource_name].hasOwnProperty('mapping_custom_relations')) {
                            entity.custom = {};
                            for(var name in details.entity[resource_name].mapping_custom_relations) {
                                entity.custom[name] = this.evaluate(details.entity[resource_name].mapping_custom_relations[name].__return__);
                            }
                        }
                })
                .then(function () {
                    that.answer_value.push(entity);
                })
            });
        })
    };

    this.get_entities = function (self, resource, count_only) {
        that = this;
        if ( !details.urls.hasOwnProperty(resource) || !details.selectors.hasOwnProperty(resource) ) {
            Messenger.error.terminate(0);
        }
        pagination = details.urls[resource].pagination ? details.urls[resource].pagination : false;

        self.then(function () {
            count = 0;
            current_page = 1;

            that.answer_value[resource] = 0;

            if (!count_only) {
                that.answer_value[resource] = {};
                that.answer_value[resource].count = 0;
                that.answer_value[resource].items = [];
            }

            else {
                that.answer_value[resource] = 0;
            }

            limit = details.limits[resource];

            self.label("GET_COUNT_LOOP_START");

        }).then(function () {
            content_url = ActionManager.precompileUrl(url + details.admin_path + details.urls[resource].base + pagination, current_page);
            self.open(content_url);
        }).then(function () {

            count = self.evaluate(function (selector_global, selector_item) {
                return document.querySelectorAll(selector_global + ' ' + selector_item).length;

            }, details.selectors[resource]['global-selector'], details.selectors[resource]['item-selector']);

            if (!count_only) {
                items = self.evaluate(function (selector_global, selector_item, selector_link) {
                    return [].slice.call(document.querySelectorAll(selector_global + ' ' + selector_item + ' ' + selector_link)).map(function (item) {
                        item_to_return_href = item.getAttribute('href');
                        return item_to_return_href[0] == '/' ? item_to_return_href : '/' + item_to_return_href;
                    });
                }, details.selectors[resource]['global-selector'], details.selectors[resource]['item-selector'], details.selectors[resource]['link-selector']);

                that.answer_value[resource].items = that.answer_value[resource].items.concat(items);
            }

            if (count_only) {
                that.answer_value[resource] += count;
            }

            else {
                that.answer_value[resource].count += count;
            }

            current_page++;
        }).then(function () {
            if (pagination && (limit == count)) {
                this.goto("GET_COUNT_LOOP_START");
            }
        });
    };

    this.parse_child_nodes = function ( entity ) {
        if(entity.hasOwnProperty('child_nodes') && !!entity.child_nodes.length) {
            Messenger.info.print(entity.child_nodes);
        }
    };
}
ActionManager.actions_description = {
    resources: "Returns all resources for Source CMS",
    counts: "Returns counts for all resources, like {\"content\":\"1000\",... etc.}",
    'parse-<resource-name>-<resources-offset>-<resources-limit>': "Returns resources as JSON ( Needle params: resource name, offset, limit )",
    'grabitems-<resource-name>': "Returns information about Source Cms"
};

ActionManager.print_actions_description = function () {
    var text = "";
    for (var i in ActionManager.actions_description) {
        text += ('\t' + i + ': "' + ActionManager.actions_description[i] + "\"\n");
    }

    return text.substring(0, text.length - 2);
};

ActionManager.precompileUrl = function (selector, number) {
    calc = eval;
    return selector.replace(/##/g, number).replace(/calc(.*)/g, function ($1) {
        return eval($1);
    });
};

ActionManager.is_numeric = function(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
};

ActionManager.get_url_vars = function(url)
{
    var vars = [], hash;
    var hashes = url.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1] || '';
    }
    return vars;
};

Object.values = function (obj) {
    var values = [];

    for (var key in obj) {
        if (!(obj[key] instanceof  Function)) {
            values.push(obj[key]);
        }
    }
    
    return values;
};
