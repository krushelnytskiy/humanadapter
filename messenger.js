/**
 * Created by v.krushelnytskiy on 4/6/15.
 */

var codes = {
  // 0-99 - error messages
    0: "Undefined error, cannot proceed",
    1: "Resource cannot be found, cannot proceed"
  // 100-199 - warning messages
  // 200-299 - info messages
};

var verify_message = function( type, code ) {
    switch(type) {
        case 'error': {
            return code >= 0 && code <= 99 ? code : 0;
        }

        case 'warning': {
            return code >= 100 && code <= 199 ? code : 0;
        }

        case 'info': {
            return code >= 200 && code <= 299 ? code : 0;
        }

        default: {
            return 0;
        }
    }
};

var Messenger = {
    error: {
        print: function ( error_code ) {
            console.log(
                JSON.stringify({
                    message: codes[verify_message('error', error_code)],
                    type: 'error'
                })
            );
        },

        print_error_message: function ( msg ) {
            console.log(
                JSON.stringify({
                    message: msg,
                    type: 'error'
                })
            );
        },

        halt: function () {
            phantom.exit(1);
        },

        terminate: function ( error_code ) {
            this.print(error_code);
            this.halt();
        }
    },

    info: {
        print: function ( mixed_value ) {
            console.log(
                JSON.stringify({
                    value: mixed_value,
                    type: "info"
                })
            );
        },

        print_exit: function ( mixed_value ) {
            Messenger.info.print( mixed_value );
            phantom.exit();
        },

        print_plain: function ( text ) {
            console.log(text);
        }
    }



};